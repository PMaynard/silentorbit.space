---
title: "European Space Agency"
description: The European Space Agency is an  intergovernmental organisation of 22 member states dedicated to the exploration of space. 
tags: []
images: []
audio: []
video: []
series: []
date: 2018-11-02T15:30:10Z

acronym: ESA
country: International
country_code: 
founded: 30 May 1975
terminated: 
site_url: https://www.esa.int/
wiki_url: https://en.wikipedia.org/wiki/European_Space_Agency

---
