---
title: "UK Space Agency"
description: 
tags: []
images: []
audio: []
video: []
series: []
date: 2018-11-02T16:37:51Z

acronym:  UKSA
country: United Kingdom of Great Britain and Northern Ireland
country_code: GB
founded: 1 April 2010
site_url: https://www.gov.uk/government/organisations/uk-space-agency
wiki_url: https://en.wikipedia.org/wiki/UK_Space_Agency
---
