---
title: "Practical Space Communication"
description: Ever wondered how you could communicate with objects in space, this featured article introduces all the possible ways you and your computer can do it. 
tags: []
images: []
audio: []
video: []
series: []
date: 2018-11-06T17:26:53Z
toc: true
feature: true

author: 
    name: Peter Maynard
    www: "https://petermaynard.co.uk"
    email: "pete@port22.co.uk"
    twitter: pgmaynard
    linkedin: pgmaynard

draft: true
---

# Introduction 

Overview of equipment needed
Something about software defined radio, mostly on RTL-SDR.
Information about the types of data

# Location Information

## Automatic identification system

Automatic identification system (AIS) is used to track ship locations at sea.

- https://en.wikipedia.org/wiki/Automatic_identification_system
- https://www.rtl-sdr.com/rtl-sdr-tutorial-cheap-ais-ship-tracking/

## Automatic Dependent Surveillance – Broadcast

Automatic Dependent Surveillance – Broadcast (ADS-B) is used to broadcast a plane's location to prevent a collision.

- https://www.rtl-sdr.com/adsb-aircraft-radar-with-rtl-sdr/


## GPS

GPS is used for location based systems, and can be picked up using simple equipment. Even mobile phones can do so. 

- https://www.rtl-sdr.com/rtl-sdr-tutorial-gps-decoding-plotting
- http://www.catb.org/gpsd/gps-hacking.html

# Images of the earth

## Automatic Picture Transmission 

Automatic Picture Transmission (APT) ...

- https://en.wikipedia.org/wiki/Automatic_picture_transmission

## Low Rate Picture Transmission

Low Rate Picture Transmission (LRPT) ...

- https://www.rtl-sdr.com/a-complete-linux-based-receiver-and-decoder-application-for-meteor-m2/
- https://en.wikipedia.org/wiki/Low-rate_picture_transmission

## High Resolution Picture Transmission 

High Resolution Picture Transmission (HRPT) ...

Needs a high gain antenna, such as a small satellite dish.

- https://en.wikipedia.org/wiki/High-resolution_picture_transmission

# Misc

## Automatic Packet Reporting System

- https://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System 

## Delay Tolerant Networking

Delay Tolerant Networking (DTN) is designed to be used as a space capible internet.

- https://en.wikipedia.org/wiki/Delay-tolerant_networking

